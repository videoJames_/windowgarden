﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherController : MonoBehaviour
{
    public Animator sun;
    public Material skyMat;
    public AnimationCurve exposure, atmosphereThickness;

    public bool debugMode= false;
    [Range(0,1)]
    public float debugTime;

    private void Start()
    {
        float dayPercent = (float)System.DateTime.Now.TimeOfDay.TotalHours;
        dayPercent /= 24;
        sun.SetFloat("offset", dayPercent);
        sun.Play("sunAnim");
        skyMat.SetFloat("_Exposure", exposure.Evaluate(dayPercent));
        skyMat.SetFloat("_AtmosphereThickness", atmosphereThickness.Evaluate(dayPercent));
    }

    void Debug()
    {
        sun.SetFloat("offset", debugTime);
        sun.Play("sunAnim");
        skyMat.SetFloat("_Exposure", exposure.Evaluate(debugTime));
        skyMat.SetFloat("_AtmosphereThickness", atmosphereThickness.Evaluate(debugTime));
    }

    private void Update()
    {
        if(debugMode)
            Debug();
    }
}
