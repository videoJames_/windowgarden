﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlaceObject : MonoBehaviour
{
    public float rotateSpeed, castHeight;

    public Mesh SeedPreviewMesh;

    NavMeshAgent n;
    float slopeHeight = .25f;
    bool wasPlacing = false;
    bool canPlace, couldPlace;
    
    public Transform originalObject;

    Transform cursor;
    MeshRenderer mr;
    MeshFilter mf;
    BoxCollider cursorCol;
    public Collider playerSurface = null;

    void Start()
    {
        n = GetComponent<NavMeshAgent>();
        n.updatePosition = false;
        cursor = transform.GetChild(0);
        mr = cursor.GetComponent<MeshRenderer>();
        mf = cursor.GetComponent<MeshFilter>();
        PlaceableObjectManager.POM.cursor = this;
    }

    public void ResetMovedObject()
    {
        originalObject.gameObject.SetActive(true);
    }
    
    void PositionObject(Vector2 size, Vector3 targetPos, Collider surf)
    {
        canPlace = true;
        if (size.magnitude > 0.1f)
        {
            RaycastHit boxHit;
            float castFloor = .15f;

            //check that there isn't another object in the way
            if (Physics.BoxCast(targetPos + new Vector3(0, castFloor + castHeight, 0), new Vector3(size.x, .1f, size.y), -transform.up, out boxHit, transform.rotation, castHeight, ~(1 << LayerMask.NameToLayer("BlockSunlightOnly"))))
            {
                if (surf == null || (surf != null && boxHit.collider != surf))
                    canPlace = false;
                Debug.DrawLine(targetPos, boxHit.point,Color.green);
            }
            
            if(canPlace)//check that 2 opposite corners aren't dangling over a ledge
            {
                Vector3 testPos = transform.TransformPoint(new Vector3(size.x, .1f, size.y));
                if (Physics.Raycast(testPos, -transform.up, out boxHit))
                {
                    if (Mathf.Abs(boxHit.point.y - targetPos.y) > slopeHeight)
                    {
                        canPlace = false;
                        Debug.DrawLine(testPos, boxHit.point);
                    }
                    else
                    {
                        testPos = transform.TransformPoint(new Vector3(-size.x, .1f, -size.y));
                        if (Physics.Raycast(testPos, -transform.up, out boxHit))
                        {
                            if (Mathf.Abs(boxHit.point.y - targetPos.y) > slopeHeight)
                            {
                                canPlace = false;
                                Debug.DrawLine(testPos, boxHit.point);
                            }
                        }
                    }
                }
            }
        }
    }

    void PlacePlantCursor()
    {
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f || Mathf.Abs(Input.GetAxis("Mouse X")) > 0.01f || Mathf.Abs(Input.GetAxis("Mouse Y")) > 0.01f || Input.GetKey(KeyCode.Space))
        {
            RaycastHit hit;
            Vector3 targetPos = Vector3.zero;
            playerSurface = null;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 15, 1 << LayerMask.NameToLayer("Soil"))) //check for soil where mouse is pointing
            {
                playerSurface = hit.collider;
                targetPos = hit.point;

                canPlace = true;

                transform.position = targetPos;
                if(!couldPlace)
                {
                    mr.materials = new Material[0];
                    mr.material = PlaceableObjectManager.POM.viableMat;
                    couldPlace = true;
                }

            }
            else if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 15)) //check for anything
            {
                if (couldPlace)
                {
                    mr.materials = new Material[0];
                    mr.material = PlaceableObjectManager.POM.badMat;
                    couldPlace = false;
                }
                canPlace = false;
                transform.position = hit.point;
            }

        }

        if (canPlace && Input.GetButtonDown("Fire1"))
        {
                CreateNewPlant(playerSurface);
        }
    }

    void CreateNewPlant(Collider surf)
    {

        if (surf != null)
        {
            Transform newObject = Instantiate(PlaceableObjectManager.POM.selectedPlant.plantPrefab, cursor.position, cursor.rotation);
            newObject.SetParent(surf.transform.parent);
            newObject.rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);

            PlacedPlant pl = newObject.GetComponent<PlacedPlant>();
            pl.parentID = surf.GetComponentInParent<PlacedObject>().ID;
            Debug.Log("Plant parent " + pl.parentID);
            pl.Create();

            newObject.gameObject.layer = LayerMask.NameToLayer("Plant");
        }
        

    }

    void CreateNewObject(Collider playerSurface)
    {
        PlaceableObjectManager.POM.placeMode = PlaceableObjectManager.Mode.Off;
        mr.materials = PlaceableObjectManager.POM.selectedObject.materials.ToArray();
        Transform newObject = Instantiate(PlaceableObjectManager.POM.objectPrefab, cursor.position, cursor.rotation);

        newObject.GetComponent<MeshFilter>().mesh = cursor.GetComponent<MeshFilter>().mesh;
        newObject.GetComponent<MeshRenderer>().materials = cursor.GetComponent<MeshRenderer>().materials;

        newObject.gameObject.layer = LayerMask.NameToLayer("PlacedObject");
        PlacedObject p = newObject.gameObject.AddComponent<PlacedObject>();
        p.ID = PlaceableObjectManager.POM.objectsPlaced++;
        p.objData = PlaceableObjectManager.POM.selectedObject;
        newObject.gameObject.name = p.objData.name;

        if (playerSurface != null)
        {
            newObject.SetParent(playerSurface.transform.parent);
            p.parentID = playerSurface.transform.parent.GetComponent<PlacedObject>().ID;
        }
        else
        {
            p.parentID = -1;
                newObject.SetParent(GameObject.Find("ObjectHolder").transform);
        }

        if (PlaceableObjectManager.POM.selectedObject.soilMesh != null)//add soil
        {
            GameObject g = new GameObject("soil");
            MeshFilter newMeshFilter = g.AddComponent<MeshFilter>();
            newMeshFilter.mesh = PlaceableObjectManager.POM.selectedObject.soilMesh;
            g.transform.parent = newObject;
            g.transform.localPosition = Vector3.zero;
            g.transform.localRotation = Quaternion.identity;
            g.AddComponent<MeshCollider>();
            g.layer = LayerMask.NameToLayer("Soil");
        }

        if (PlaceableObjectManager.POM.selectedObject.surfaceMesh != null)//add surface
        {
            GameObject g = new GameObject("surface");
            MeshFilter newMeshFilter = g.AddComponent<MeshFilter>();
            newMeshFilter.mesh = PlaceableObjectManager.POM.selectedObject.surfaceMesh;
            g.transform.parent = newObject;
            g.transform.localPosition = Vector3.zero;
            g.transform.localRotation = Quaternion.identity;
            g.AddComponent<MeshCollider>();
            g.layer = LayerMask.NameToLayer("PlacedSurface");
        }
        else
        {
            newObject.gameObject.AddComponent<BoxCollider>();
        }
    }

    void PlaceCursor()
    {
        transform.Rotate(new Vector3(0, Time.deltaTime * Input.GetAxis("Horizontal") * rotateSpeed));

        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f || Mathf.Abs(Input.GetAxis("Mouse X")) > 0.01f || Mathf.Abs(Input.GetAxis("Mouse Y")) > 0.01f || Input.GetKey(KeyCode.Space))
        {
            RaycastHit hit;
            Vector3 targetPos = Vector3.zero;
            bool validPoint = false;

            playerSurface = null;
            Vector2 size = PlaceableObjectManager.POM.selectedObject.size;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 15, 1 << LayerMask.NameToLayer("PlacedSurface"))) //check for placed surface first
            {
                playerSurface = hit.collider;
                targetPos = hit.point;
                validPoint = true;
            }
            else if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 15, 1 << LayerMask.NameToLayer("Surface"))) //check floor
            {
                NavMeshHit nh;

                NavMesh.SamplePosition(hit.point, out nh, 100, 1); //derive navmesh position from mouse position
                targetPos = nh.position;
                validPoint = true;
            }

            if (validPoint)
            {

                PositionObject(size, targetPos,playerSurface);

                if (canPlace)
                {
                    if (!couldPlace)
                    {
                        couldPlace = true;
                        Material[] mats = mr.materials;
                        int i = 0;
                        foreach (Material m in mats)
                        {
                            mats[i] = PlaceableObjectManager.POM.viableMat;
                            i++;
                        }
                        mr.materials = mats;
                    }
                }
                else if (couldPlace)
                {
                    couldPlace = false;
                    Material[] mats = mr.materials;
                    int i = 0;
                    foreach (Material m in mats)
                    {
                        mats[i] = PlaceableObjectManager.POM.badMat;
                        i++;
                    }
                    mr.materials = mats;
                }

                n.Warp(targetPos);
            }
        }

        if (canPlace && Input.GetButtonDown("Fire1"))
        {
            if (PlaceableObjectManager.POM.placeMode == PlaceableObjectManager.Mode.SurfaceObject)
                CreateNewObject(playerSurface);
            else
                MoveSelectedObject(playerSurface);
            playerSurface = null;
        }
    }

    void MoveSelectedObject(Collider playerSurface)
    {
        PlaceableObjectManager.POM.placeMode = PlaceableObjectManager.Mode.Off;
        mr.materials = PlaceableObjectManager.POM.selectedObject.materials.ToArray();

        originalObject.gameObject.SetActive(true);
        originalObject.position = cursor.position;
        originalObject.rotation = cursor.rotation;
        if (playerSurface != null)
        {
            originalObject.GetComponent<PlacedObject>().parentID = playerSurface.transform.parent.GetComponent<PlacedObject>().ID;
            originalObject.SetParent(playerSurface.transform.parent);
        }
        else
        {
            originalObject.GetComponent<PlacedObject>().parentID = -1;
            originalObject.SetParent(GameObject.Find("ObjectHolder").transform);
        }

        PlacedPlant[] p = originalObject.GetComponentsInChildren<PlacedPlant>();
        for(int i = 0; i < p.Length; i++)
        {
            p[i].Move();
        }
    }

    void Update()
    {
        Material[] mats;
        int i = 0;

        switch (PlaceableObjectManager.POM.placeMode)
        {
            case PlaceableObjectManager.Mode.SoilObject:
                if (PlaceableObjectManager.POM.selectionChanged)
                {
                    mf.mesh = SeedPreviewMesh;
                    mr.material = PlaceableObjectManager.POM.badMat;
                    canPlace = false;
                    couldPlace = false;

                    if (!wasPlacing)
                    {
                        mr.enabled = true;
                        wasPlacing = true;
                    }
                }
                PlacePlantCursor();
                break;

            case PlaceableObjectManager.Mode.MoveSurfaceObject: // move existing object

                if (PlaceableObjectManager.POM.selectionChanged)
                {
                    n.Warp(originalObject.position);

                    mf.mesh = PlaceableObjectManager.POM.selectedObject.mesh;
                    mr.materials = PlaceableObjectManager.POM.selectedObject.materials.ToArray();
                    mats = mr.materials;
                    foreach (Material m in mats)
                    {
                        mats[i] = PlaceableObjectManager.POM.badMat;
                        i++;
                    }
                    mr.materials = mats;
                    canPlace = false;
                    couldPlace = false;

                    if (!wasPlacing)
                    {
                        mr.enabled = true;
                        wasPlacing = true;
                    }
                }
                    PlaceCursor();
                break;

            case PlaceableObjectManager.Mode.SurfaceObject: //new object

                if (PlaceableObjectManager.POM.selectionChanged)
                {

                    mf.mesh = PlaceableObjectManager.POM.selectedObject.mesh;
                    mr.materials = PlaceableObjectManager.POM.selectedObject.materials.ToArray();
                    mats = mr.materials;
                    i = 0;
                    foreach (Material m in mats)
                    {
                        mats[i] = PlaceableObjectManager.POM.badMat;
                        i++;
                    }
                    mr.materials = mats;
                    canPlace = false;
                    couldPlace = false;

                    if (!wasPlacing)
                    {
                        mr.enabled = true;
                        wasPlacing = true;
                    }
                }
                    PlaceCursor();
                break;

            case PlaceableObjectManager.Mode.Off:
                if(wasPlacing)
                {
                    WorldUI.UIm.Deselect();
                    mr.enabled = false;
                    wasPlacing = false;
                }
                break;
        }
    }
}
