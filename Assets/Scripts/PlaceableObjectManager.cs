﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceableObjectManager : MonoBehaviour
{
    public Transform objectPrefab;

    public enum Mode { Off, SurfaceObject, MoveSurfaceObject, SoilObject, MoveSoilObject };

    public int objectsPlaced;
    public static PlaceableObjectManager POM;
    public List<PlaceableObject> availableObjects;
    public PlaceableObject selectedObject;
    public PlantData selectedPlant;

    public bool selectionChanged;
    public Mode placeMode;
    public Material viableMat, badMat;

    int objIndex = 0;
    public PlaceObject cursor;

    public void LoadSavedObjects()
    {
        PlacedObject[] allObjects = new PlacedObject[SaveManager.sm.SavedPlaceableObjects.Count]; //load placed objects
        foreach(SavedPlacedObject s in SaveManager.sm.SavedPlaceableObjects)
        {
            GameObject newObj = Instantiate(objectPrefab).gameObject;
            newObj.name = s.dataObj;

            PlacedObject p = newObj.AddComponent<PlacedObject>();
            p.objData = (PlaceableObject)Resources.Load("Objects/" + s.dataObj);
            p.ID = s.ID;
            p.parentID = s.parentID;

            newObj.GetComponent<MeshFilter>().mesh = p.objData.mesh;
            newObj.GetComponent<MeshRenderer>().materials = p.objData.materials.ToArray();

            newObj.transform.position = new Vector3(s.posX, s.posY, s.posZ);
            newObj.transform.rotation = new Quaternion(s.rotX, s.rotY, s.rotZ, s.rotW);

            newObj.layer = LayerMask.NameToLayer("PlacedObject");

            if (p.objData.soilMesh != null)//add soil
            {
                GameObject g = new GameObject("soil");
                MeshFilter newMeshFilter = g.AddComponent<MeshFilter>();
                newMeshFilter.mesh = p.objData.soilMesh;
                g.transform.parent = newObj.transform;
                g.transform.localPosition = Vector3.zero;
                g.transform.localRotation = Quaternion.identity;
                g.AddComponent<MeshCollider>();
                g.layer = LayerMask.NameToLayer("Soil");
            }

            if (p.objData.surfaceMesh != null)//add surface
            {
                GameObject g = new GameObject("surface");
                MeshFilter newMeshFilter = g.AddComponent<MeshFilter>();
                newMeshFilter.mesh = p.objData.surfaceMesh;
                g.transform.parent = newObj.transform;
                g.transform.localPosition = Vector3.zero;
                g.transform.localRotation = Quaternion.identity;
                g.AddComponent<MeshCollider>();
                g.layer = LayerMask.NameToLayer("PlacedSurface");
            }
            else
            {
                newObj.gameObject.AddComponent<BoxCollider>();
            }

            if (p.ID > objectsPlaced - 1)
                objectsPlaced = p.ID + 1;

            allObjects[p.ID] = p;
        }

        for (int i = 0; i < allObjects.Length; i++) //fix parenting
        {
            if (allObjects[i].GetComponent<PlacedObject>().parentID >= 0)
                allObjects[i].transform.SetParent(allObjects[allObjects[i].GetComponent<PlacedObject>().parentID].transform);
            else
                allObjects[i].transform.SetParent(GameObject.Find("ObjectHolder").transform);
        }

        //load plants
        foreach (SavedPlacedPlant s in SaveManager.sm.SavedPlaceablePlants)
        {
            PlantData obj = (PlantData)Resources.Load("Objects/plants/" + s.dataObj);
            Transform newPlant = Instantiate(obj.plantPrefab);

            newPlant.gameObject.layer = LayerMask.NameToLayer("Plant");
            newPlant.position = new Vector3(s.posX, s.posY, s.posZ);
            newPlant.rotation = new Quaternion(s.rotX, s.rotY, s.rotZ, s.rotW);

            newPlant.SetParent(allObjects[s.parentID].transform);

            PlacedPlant pl = newPlant.GetComponent<PlacedPlant>(); //fill out plant info
            pl.dateCreated = s.dateCreated;
            pl.lastWatered = s.lastWatered;
            pl.hasDied = s.hasDied;
            pl.lastGrown = s.lastGrown;
            pl.currentHealth = s.currentHealth;
            pl.currentGrowth = s.currentGrowth;

            pl.parentID = s.parentID;

            pl.Load();
        }
    }

    public void SelectCreatedObject(PlaceableObject newSelect)
    {
        selectedObject = newSelect;
        selectionChanged = true;
        placeMode = Mode.MoveSurfaceObject;
        StartCoroutine(Selected());
    }

    IEnumerator Selected()
    {
        yield return 0;
        selectionChanged = false;
    }

    void Awake()
    {
        POM = this;
        selectedObject = availableObjects[objIndex];
    }

    void Update()
    {
        if (false && Input.GetKeyDown(KeyCode.Space))
        {
            if (placeMode != Mode.Off)
            {
                if (placeMode == Mode.MoveSurfaceObject || placeMode == Mode.MoveSoilObject)
                    cursor.ResetMovedObject();
                placeMode = Mode.Off;
            }
            else if (placeMode == Mode.Off)
            {
                placeMode = Mode.SurfaceObject;
                selectionChanged = true;
            }
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            if (placeMode != Mode.SoilObject)
            {
                if (placeMode == Mode.MoveSurfaceObject || placeMode == Mode.MoveSoilObject)
                    cursor.ResetMovedObject();
                placeMode = Mode.SoilObject;
                selectionChanged = true;
            }
            else if(placeMode == Mode.SoilObject)
            {
                placeMode = Mode.Off;
            }
        }
        else if (placeMode == Mode.SurfaceObject)
        {
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                selectionChanged = true;
                objIndex++;
                objIndex %= availableObjects.Count;
                selectedObject = availableObjects[objIndex];
            }
            else if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                selectionChanged = true;
                objIndex--;
                if (objIndex < 0)
                    objIndex = availableObjects.Count - 1;
                selectedObject = availableObjects[objIndex];
            }
            else
            {
                selectionChanged = false;
            }
        }
        else if (placeMode == Mode.SoilObject)
        {
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                
            }
            else if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                
            }
            else
            {
                selectionChanged = false;
            }
        }
    }
}