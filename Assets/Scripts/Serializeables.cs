﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SaveData
{
    public List<SavedPlacedObject> objects;
    public List<SavedPlacedPlant> plants;
}

[Serializable]
public class SavePlaced
{
    public string dataObj;
    public int parentID;
    public float posX, posY, posZ;
    public float rotX, rotY, rotZ, rotW;
}

[Serializable]
public class SavedPlacedObject : SavePlaced
{
    public int ID;
}

[Serializable]
public class SavedPlacedPlant : SavePlaced
{
    public float currentHealth, currentGrowth;
    public DateTime dateCreated, lastWatered, lastGrown;
    public bool hasDied;
}
