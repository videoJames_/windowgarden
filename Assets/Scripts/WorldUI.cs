﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldUI : MonoBehaviour
{
    public static WorldUI UIm;

    enum UIstate { off, objectSelected };
    UIstate currentState;

    private void Awake()
    {
        UIm = this;
    }

    public void SelectObject(Transform placedObject)
    {
        currentState = UIstate.objectSelected;
    }

    public void Deselect()
    {
        currentState = UIstate.off;
    }

    private void Update()
    {
        if (currentState == UIstate.off)
        {
            RaycastHit[] hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(Input.mousePosition), 15, 1 << LayerMask.NameToLayer("ObjUI"));

            foreach (RaycastHit r in hits)
            {
                r.collider.GetComponent<ObjectSelector>().Hover();
            }
        }
    }

    public void MoveObject(Transform obj)
    {
        PlaceableObjectManager.POM.cursor.originalObject = obj;

        PlaceableObjectManager.POM.placeMode = PlaceableObjectManager.Mode.MoveSurfaceObject;
        PlaceableObjectManager.POM.SelectCreatedObject(obj.GetComponent<PlacedObject>().objData);
        PlaceableObjectManager.POM.cursor.playerSurface = null;
        obj.gameObject.SetActive(false);
    }
}
