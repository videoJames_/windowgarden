﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDisplay : MonoBehaviour {

    public Image im;
    public MeshFilter mf;
    public MeshRenderer mr;
    public Text tx;
}
