﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PlantData")]
public class PlantData : ScriptableObject {

    public float waterNeedHours; //how many hours til thirst bar is full again
    [Range(0,100)]
     public float sunNeed;//0 to 100
    public float growthTime,//in hours
        deathTime;//how long before neglected plant reaches zero health. also the amount of time it takes to heal?
    public Transform plantPrefab;

    public List<Mesh> plantMeshes;
    public List<MaterialList> plantMaterials;
}

[System.Serializable]
public class MaterialList
{
    public List<Material> matList;
}
