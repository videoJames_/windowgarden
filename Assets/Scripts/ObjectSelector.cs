﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ObjectSelector : MonoBehaviour {

    enum SelectState { off, hover, selected};
    SelectState currentState;
    Image button;
    public GameObject subMenu;

    float hoverTimer;

    
    private void Start()
    {
        button = GetComponent<Image>();
        GetComponent<Button>().onClick.AddListener(SelectMe);
        subMenu.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(MoveMe);
        subMenu.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(DeleteMe);
        subMenu.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(CancelMenu);
        subMenu.SetActive(false);

    }

    void SelectMe()
    {
        currentState = SelectState.selected;
        WorldUI.UIm.SelectObject(transform.parent);
    }

    void MoveMe()
    {
        Debug.Log("move");
        WorldUI.UIm.MoveObject(transform.parent);
        currentState = SelectState.off;
    }

    void CancelMenu()
    {
        WorldUI.UIm.Deselect();
        currentState = SelectState.off;
    }

    void DeleteMe()
    {
        Debug.Log("delet");

    }

    public void Hover()
    {
        hoverTimer = .15f;
    }

    private void LateUpdate()
    {
        if(currentState != SelectState.selected)
        {
            hoverTimer -= Time.deltaTime;
            if (hoverTimer <= 0)
                currentState = SelectState.off;
            else
                currentState = SelectState.hover;
        }

        switch(currentState)
        {
            case SelectState.off:
                button.enabled = false;
                subMenu.SetActive(false);
                break;

            case SelectState.hover:
                button.enabled = true;
                subMenu.SetActive(false);
                break;

            case SelectState.selected:
                button.enabled = false;
                subMenu.SetActive(true);
                break;
        }

        transform.LookAt(Camera.main.transform);
        subMenu.transform.LookAt(Camera.main.transform);
    }
}
