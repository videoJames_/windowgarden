﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public static MenuController mc;
    public enum MenuState { off, obj };

    public MenuState curState = MenuState.off;

    MenuState lastState;
    Animator anim;

    public RectTransform meshZone;
    public Canvas objCanvas;

    public List<PlaceableObject> potList, furnitureList, junkList;
    public List<ItemDisplay> itemPage;

    public GameObject scrollbar;
    public List<Toggle> scrollDots;

    public Scrollbar sb;
    int tab, pages;
    int[] currentPage = new int[3];
    float scrollSpeed = 15f;


    private void Awake()
    {
        mc = this;
        anim = GetComponent<Animator>();
        
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (curState == MenuState.off)
                curState = MenuState.obj;
            else if (curState == MenuState.obj)
                curState = MenuState.off;
        }

        switch (curState)
        {
            case MenuState.off:
                anim.SetBool("storeMenu", false);
                if (lastState != curState)
                    DisableItemPreviews();
                lastState = MenuState.off;
                break;
            default:
                anim.SetBool("storeMenu", true);
                if (lastState != curState)
                {
                    DisplayNewTab();
                }
                CheckItemPreviews();
                lastState = curState;
                break;
        }
    }

    void DisableItemPreviews()
    {
        foreach (ItemDisplay p in itemPage)
        {
            p.im.enabled = false;
            p.tx.text = "";
            p.mr.enabled = false;
        }
    }

    void CheckItemPreviews()
    {
        Rect r = new Rect(0, objCanvas.worldCamera.WorldToScreenPoint(meshZone.position).y, Screen.width,meshZone.rect.height);

        foreach (ItemDisplay p in itemPage)
        {
            if(p.im.enabled)
            {
                if(!r.Contains(objCanvas.worldCamera.WorldToScreenPoint(p.mr.transform.parent.position)))
                {
                    p.mr.enabled = false;
                }
                else
                {
                    p.mr.enabled = true;
                }
            }
        }
    }

    public void CloseMenu()
    {
        curState = MenuState.off;
    }

    public void SetTab(int n)
    {
        tab = n;
        DisplayNewTab();
    }

    void DisplayNewTab()
    {
        //determine how many pages long the tab is
        if (tab == 0)
        {
            pages = 1 + Mathf.CeilToInt(potList.Count / 6);
        }
        else if (tab == 1)
        {
            pages = 1 + Mathf.CeilToInt(furnitureList.Count / 6);
        }
        else if (tab == 2)
        {
            pages = 1 + Mathf.CeilToInt(junkList.Count / 6);
        }

        //activate scrollbar if appropriate
        if (pages > 1)
        {
            scrollbar.SetActive(true);
            int i = pages;
            foreach (Toggle t in scrollDots)
            {
                if (i > 0)
                    t.gameObject.SetActive(true);
                else
                    t.gameObject.SetActive(false);
                t.isOn = false;
                i--;
            }
            scrollDots[currentPage[tab]].isOn = true;
        }
        else
        {
            scrollbar.SetActive(false);
        }
        DisplayNewPage();
    }

    public void SetPage(int n)
    {

        if (n >= 0 && n < pages)
        {
            bool r = (n > currentPage[tab]);
            currentPage[tab] = n;
            StartCoroutine(ItemScroll(r));
        }
        scrollDots[currentPage[tab]].isOn = true;
    }

    public void ChangePage(bool up)
    {
        if (!up && currentPage[tab] > 0)
        {
            currentPage[tab]--;
            StartCoroutine(ItemScroll(false));
        }
        else if (up && currentPage[tab] < pages - 1)
        {
            currentPage[tab]++;
            StartCoroutine(ItemScroll(true));
        }
        scrollDots[currentPage[tab]].isOn = true;

    }

    void DisplayNewPage()
    {
        List<PlaceableObject> l = new List<PlaceableObject>();
        if (tab == 0)
        {
            l = potList;
        }
        else if (tab == 1)
        {
            l = furnitureList;
        }
        else if (tab == 2)
        {
            l = junkList;
        }

        DisableItemPreviews();
        int i = 6 * (currentPage[tab]);
        foreach (ItemDisplay p in itemPage)
        {
            if (i < l.Count)
            {
                p.im.enabled = true;
                p.tx.text = l[i].sName;
                p.mr.enabled = true;
                p.mr.materials = l[i].materials.ToArray();
                p.mf.mesh = l[i].mesh;
                p.mf.transform.localScale = Vector3.one * l[i].displayScale;
            }
            i++;
        }
    }
    
    void DisplayHalfPage(bool up)
    {
        List<PlaceableObject> l = new List<PlaceableObject>();
        if (tab == 0)
        {
            l = potList;
        }
        else if (tab == 1)
        {
            l = furnitureList;
        }
        else if (tab == 2)
        {
            l = junkList;
        }

        DisableItemPreviews();
        if (up)
        {

            int i = 6 * (currentPage[tab]);
            i += 3;
            foreach (ItemDisplay p in itemPage)
            {
                if (i < l.Count && i >= 0)
                {
                    p.im.enabled = true;
                    p.tx.text = l[i].sName;
                    p.mr.enabled = true;
                    p.mr.materials = l[i].materials.ToArray();
                    p.mf.mesh = l[i].mesh;
                    p.mf.transform.localScale = Vector3.one * l[i].displayScale;
                }
                i++;
            }
        }
        else
        {
            int i = 6 * (currentPage[tab]);
            i -= 3;
            foreach (ItemDisplay p in itemPage)
            {
                if (i < l.Count && i >= 0)
                {
                    p.im.enabled = true;
                    p.tx.text = l[i].sName;
                    p.mr.enabled = true;
                    p.mr.materials = l[i].materials.ToArray();
                    p.mf.mesh = l[i].mesh;
                    p.mf.transform.localScale = Vector3.one * l[i].displayScale;
                }
                i++;
            }
        }
    }

    IEnumerator ItemScroll(bool up)
    {
        if (up)
        {
            sb.value = 1;
            while (sb.value > .01f)
            {
                sb.value -= Time.deltaTime * scrollSpeed;
                yield return new WaitForEndOfFrame();
            }
            sb.value = 1;
            DisplayHalfPage(false);
            while (sb.value > .01f)
            {
                sb.value -= Time.deltaTime * scrollSpeed;
                yield return new WaitForEndOfFrame();
            }
            sb.value = 1;
            DisplayNewPage();
        }
        else
        {
            sb.value = 0;
            DisplayHalfPage(true);
            while (sb.value < .99f)
            {
                sb.value += Time.deltaTime * scrollSpeed;
                yield return new WaitForEndOfFrame();
            }
            sb.value = 0;
            DisplayNewPage();
            while (sb.value < .99f)
            {
                sb.value += Time.deltaTime * scrollSpeed;
                yield return new WaitForEndOfFrame();
            }
            sb.value = 1;
        }

    }
}
