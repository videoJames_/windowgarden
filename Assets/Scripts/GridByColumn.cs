﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[ExecuteInEditMode]
public class GridByColumn : MonoBehaviour
{
    RectTransform r;
    GridLayoutGroup g;
    public Vector2 units;

    private void Awake()
    {
        r = GetComponent<RectTransform>();
        g = GetComponent<GridLayoutGroup>();
    }

    // Update is called once per frame
    void Update ()
    {
        if(r == null)
            r = GetComponent<RectTransform>();
        if (g == null)
            g = GetComponent<GridLayoutGroup>();

        if(r != null && g != null && !Application.isPlaying)
        {
                 g.cellSize = new Vector2((r.rect.width - (units.x * g.spacing.x)) / units.x , (r.rect.height - (units.y * g.spacing.y)) / units.y);
            
        }
    }
}
