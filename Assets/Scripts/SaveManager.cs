﻿using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour {

    public static SaveManager sm;

    public List<SavedPlacedObject> SavedPlaceableObjects;
    public List<SavedPlacedPlant> SavedPlaceablePlants;

    public delegate void SaveDelegate(object sender, EventArgs args);
    public event SaveDelegate SaveEvent;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SaveAndQuit();
        }
        
    }

    void LoadGame()
    {
        SaveData d;
        if (!Directory.Exists("Saves"))
        {
            Debug.LogException(new Exception("no save directory!"));
        }
        else
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream saveFile = File.Open("Saves/save.binary", FileMode.Open);

            d = (SaveData)bf.Deserialize(saveFile);

            saveFile.Close();

            SavedPlaceableObjects = d.objects;
            SavedPlaceablePlants = d.plants;

            PlaceableObjectManager.POM.LoadSavedObjects();
        }

    }

    void SaveGame()
    {
        if (!Directory.Exists("Saves"))
            Directory.CreateDirectory("Saves");

        SavedPlaceableObjects = new List<SavedPlacedObject>();
        SavedPlaceablePlants = new List<SavedPlacedPlant>();

        if (SaveEvent != null)
            SaveEvent(null, null); //gathers lists of everything in the scene

        SaveData d = new SaveData();
        d.objects = SavedPlaceableObjects;
        d.plants = SavedPlaceablePlants;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream saveFile = File.Create("Saves/save.binary");

        bf.Serialize(saveFile, d);

        saveFile.Close();

    }

    void SaveAndQuit()
    {
        SaveGame();
        Application.Quit();
    }

    private void Start()
    {
        LoadGame();
    }

    private void Awake()
    {
        {
            sm = this;
        }
    }

}
