﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollCamera : MonoBehaviour {

    public List<Transform> RailObjects, TargetRail;
    public float scrollSpeed, zoomSpeed, vertSpeed, dragZone;

    float leftToRight = .5f, verticalLookOffset, verticalPosOffset, zoomFactor, currentScroll, currentTilt, currentVertPos, targetZoom;

    float xInput, yInput;

    Vector2 dragStartPos;
    
    void Update ()
    {
        GetInput();
        Scroll();
        Look();
        PositionCamera();
    }

    void PositionCamera()
    {
        //use a bezier curve from the 3 points
        Vector3 p1 = Vector3.Lerp(RailObjects[0].position, RailObjects[1].position, leftToRight);
        Vector3 p2 = Vector3.Lerp(RailObjects[1].position, RailObjects[2].position, leftToRight);
        Vector3 p3 = Vector3.Lerp(p1, p2, leftToRight);

        p1 = Vector3.Lerp(TargetRail[0].position, TargetRail[1].position, leftToRight);
        p2 = Vector3.Lerp(TargetRail[1].position, TargetRail[2].position, leftToRight);
        Vector3 p4 = Vector3.Lerp(p1, p2, leftToRight) + new Vector3(0,verticalLookOffset,0);

        Vector3 p5 = Vector3.Lerp(p3, p4, zoomFactor);
        transform.position = new Vector3(0, verticalPosOffset, 0) + new Vector3(p5.x, p3.y, p5.z);
        transform.LookAt(p4);
    }

    void Scroll()
    {
        if(Mathf.Abs(xInput) > 0.25f)
        {
            currentScroll = Mathf.Lerp(currentScroll, -Time.deltaTime * scrollSpeed * xInput, Time.deltaTime * 5);
        }
        else
        {
            currentScroll = Mathf.Lerp(currentScroll, 0, Time.deltaTime * 3f);
        }

        leftToRight += currentScroll;
        if (leftToRight < 0)
            leftToRight = 0;
        if (leftToRight > 1)
            leftToRight = 1;
    }

    void Look()
    {
        if (Mathf.Abs(yInput)>.25f)
        {
            currentTilt = Mathf.Lerp(currentTilt, -Time.deltaTime * yInput * vertSpeed, Time.deltaTime * 5f);
            currentVertPos = Mathf.Lerp(currentVertPos, Time.deltaTime * yInput * vertSpeed, Time.deltaTime * 5f);
        }
        else
        {
            currentTilt = Mathf.Lerp(currentTilt, 0, Time.deltaTime * 3f);
            currentVertPos = Mathf.Lerp(currentVertPos, 0, Time.deltaTime * 7f);
        }
        verticalPosOffset += currentVertPos;
        bool stop = false;
        if (verticalPosOffset > .6f)
        {
            verticalPosOffset = .6f;
            stop = true;
        }
        if (verticalPosOffset < -1.7f)
        {
            verticalPosOffset = -1.7f;
            stop = true;
        }
        if(!stop)
        {
            verticalLookOffset += currentTilt;
            if (verticalLookOffset > .2f)
                verticalLookOffset = .2f;
            if (verticalLookOffset < -1.7f)
                verticalLookOffset = -1.7f;
        }
    }

    void GetInput()
    {
        if(Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
        {
            dragStartPos = Input.mousePosition;
        }
        else if(Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            Vector2 dragDir = dragStartPos - (Vector2)Input.mousePosition;
            xInput = dragDir.x / -dragZone;
            yInput = dragDir.y / (dragZone * .5f);
            dragStartPos = Input.mousePosition;
        }
        else
        {
            xInput = 0;
            yInput = 0;
        }

        targetZoom = targetZoom + Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * zoomSpeed;
        if (targetZoom < 0)
            targetZoom = 0;
        if (targetZoom > .55f)
            targetZoom = .55f;
        zoomFactor = Mathf.Lerp(zoomFactor, targetZoom, Time.deltaTime * 5f);
        
    }
}
