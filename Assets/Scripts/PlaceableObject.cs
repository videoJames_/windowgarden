﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PlaceableObject")]
public class PlaceableObject : ScriptableObject
{
    public string sName;
    public float displayScale;
    public Vector2 size;
    public Mesh mesh, surfaceMesh, soilMesh;
    public List<Material> materials;
}
