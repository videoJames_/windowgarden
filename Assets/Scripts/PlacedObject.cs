﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlacedObject : MonoBehaviour {

    public PlaceableObject objData;
    public int ID, parentID = -1;

    public void SavePlacedObject(object sender, EventArgs args)
    {
        SavedPlacedObject plo = new SavedPlacedObject();
        plo.posX = transform.position.x;
        plo.posY = transform.position.y;
        plo.posZ = transform.position.z;
        plo.rotX = transform.rotation.x;
        plo.rotY = transform.rotation.y;
        plo.rotZ = transform.rotation.z;
        plo.rotW = transform.rotation.w;
        plo.dataObj = objData.name;
        plo.ID = ID;
        plo.parentID = parentID;

        SaveManager.sm.SavedPlaceableObjects.Add(plo);
    }

    private void OnDestroy()
    {
        SaveManager.sm.SaveEvent -= SavePlacedObject;
    }

    private void Start()
    {
        SaveManager.sm.SaveEvent += SavePlacedObject;
    }
}
