﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlacedPlant : MonoBehaviour {

    public PlantData pd;

    public bool debugWater, debugMode;

    MeshFilter mf;
    MeshRenderer mr;
    Text statusText;

    public float currentHealth, //-1 to 1
        currentGrowth;
     float   currentThirst, currentSunlight = -10;
    public DateTime dateCreated, lastWatered, lastGrown;
    public float age;
    int stage, wasStage = -1;

    public int parentID = -1;
    float growTimeMultiplier;

    public bool hasDied;

    private void OnDestroy()
    {
        SaveManager.sm.SaveEvent -= SavePlant;
    }

    public void Load()
    {
        StartCoroutine(CalculateSunlight());
     //   statusText.transform.SetParent(GameObject.Find("StatCanvas").transform);
        statusText.transform.rotation = Quaternion.identity;
    }

    public void Create()
    {
     //   statusText.transform.SetParent(GameObject.Find("StatCanvas").transform);
        statusText.transform.rotation = Quaternion.identity;

        currentHealth = 1;
        dateCreated = System.DateTime.Now;
        Water();
        Move();

    }

    void SavePlant(object sender, EventArgs args)
    {
        SavedPlacedPlant plo = new SavedPlacedPlant();
        plo.posX = transform.position.x;
        plo.posY = transform.position.y;
        plo.posZ = transform.position.z;
        plo.rotX = transform.rotation.x;
        plo.rotY = transform.rotation.y;
        plo.rotZ = transform.rotation.z;
        plo.rotW = transform.rotation.w;
        plo.dataObj = pd.name;
        plo.parentID = parentID;

        plo.dateCreated = dateCreated;
        plo.lastGrown = lastGrown;
        plo.lastWatered = lastWatered;
        plo.currentHealth = currentHealth;
        plo.currentGrowth = currentGrowth;
        plo.hasDied = hasDied;

        SaveManager.sm.SavedPlaceablePlants.Add(plo);
    }

    void Awake()
    {
        SaveManager.sm.SaveEvent += SavePlant;
        mf = GetComponentInChildren<MeshFilter>();
        mr = GetComponentInChildren<MeshRenderer>();
        growTimeMultiplier = UnityEngine.Random.Range(.9f, 1.1f);//randomize plant growth a little

        statusText = GetComponentInChildren<Text>();
        Water();
        CalculateTimers();
        Grow();
    }

    void Update()
    {
        CalculateTimers();
        if (Input.GetKeyDown(KeyCode.I))
            debugMode = true;
        if (Input.GetKeyDown(KeyCode.O))
            debugMode = false;


            Grow();
        if (Input.GetKeyDown(KeyCode.W))
            Water();

        if(debugWater)
        {
            debugWater = false;
            Water();
        }
    }

    
    public void Move()
    {
        statusText.transform.position = transform.position + new Vector3(0, .2f, 0);
        statusText.transform.rotation = Quaternion.identity;
        StartCoroutine(CalculateSunlight());
    }

    public void Water()
    {
        lastWatered = System.DateTime.Now;
        currentThirst -= 50;
        if (currentThirst < 0)
            currentThirst = 0;
    }

    void ChoosePlantMesh()
    {
        bool found = false;
        for(int i = 0; i < pd.plantMeshes.Count; i++)
        {
            if(!found)
            {
                if(currentGrowth < (i + 1) * (1f / pd.plantMeshes.Count))
                {
                    stage = i;
                    found = true;
                }
            }
        }

        if (!found)
            stage = pd.plantMeshes.Count - 1;

        if(stage != wasStage)
        {
            mf.mesh = pd.plantMeshes[stage];
            mr.materials = pd.plantMaterials[stage].matList.ToArray();
            wasStage = stage;
        }
    }

    void Grow()//runs at start of session
    {
        CalculateHealth();
        float totalGrowTime = pd.growthTime * growTimeMultiplier * 3600;//how many seconds it takes to grow fully
        float sunDiff = Mathf.Abs(currentSunlight - pd.sunNeed);//bigger means less ideal sunlight (0-100)
        float timeSinceLastGrown = (float)(System.DateTime.Now - lastGrown).TotalSeconds;

        if (currentHealth > 0) //only healthy plant grow
        {
            if(currentThirst < 85) //only not thirsty plant grow
            {
                if(sunDiff < 20)
                {
                    currentGrowth += timeSinceLastGrown / totalGrowTime;
                }
                else
                {
                    currentGrowth += (timeSinceLastGrown * Mathf.Clamp01(1 - ((sunDiff - 30)/80))) / totalGrowTime;//grow less fast if poorly lit
                }
            }
            else
            {
                //grow from time it was not thirsty
                if(lastGrown < lastWatered)
                {
                    //grow by 85% of the water cycle
                    currentGrowth += (pd.waterNeedHours * 3600 * .85f) / totalGrowTime;
                }
            }
            
        }
        //note that this means some growth can be lost if your plant becomes uneh
        

        ChoosePlantMesh();
        lastGrown = DateTime.Now;
    }



    IEnumerator CalculateSunlight()
    {

        Vector3 basePos = transform.position + new Vector3(0, .6f, 0);
        int tests = 30;

        while(tests > 0)
        {
            Vector3 dir = Vector3.right;
            Vector3 dir2 = Vector3.up;

            int lightRays = 0;//used to count unblocked rays

            //choose a segment of the full sphere to test
            float ray = UnityEngine.Random.Range(30f, 50f);
            int q = Mathf.FloorToInt(ray / 3);
            int s = q * UnityEngine.Random.Range(0, 3);
            int casts = q;

            for(int i = s; i < s + q; i++)
            {
                float off = UnityEngine.Random.Range(-3f, 3f);
                Vector3 d = Quaternion.AngleAxis((18f + off) * i, dir2) * dir;

                if (Vector3.Dot(new Vector3(0,-1,0),d.normalized) < .3f)//throw away downward facing rays
                {
                    Debug.DrawRay(basePos, d);

                    if (!Physics.Raycast(basePos, d, 20))//might need to be improved with a layermask
                        lightRays++;
                }
                else
                {
                    casts--;
                }

                off = UnityEngine.Random.Range(-3f, 3f);
                dir2 = Quaternion.AngleAxis(43f + off, transform.right) * dir2;
            }
            tests--;

            if(currentSunlight < 0)
                currentSunlight = (lightRays / (casts - (q / 9f))) * 100;
            else
              currentSunlight = Mathf.Lerp(currentSunlight,(lightRays / (casts - (q / 9f))) * 100f,.2f);
            yield return null;
        }
    }


    void CalculateTimers()
    {
        age =(float) (System.DateTime.Now - dateCreated).TotalSeconds;
        currentThirst = ((float)(System.DateTime.Now - lastWatered).TotalSeconds / (pd.waterNeedHours * 3600)) * 100;
    }

    void CalculateHealth()
    {
        if (currentThirst < 90 && Mathf.Abs(currentSunlight - pd.sunNeed) < 40) //if not thirsty & sun is ok, improve the plant health
        {
            //(using 9 instead of 3600 so the plant will take a small % of deathTime hrs to heal from full death)
            currentHealth += (float)(System.DateTime.Now - lastGrown).TotalSeconds / (pd.deathTime * 350);
        }
        else if(currentThirst >= 90)
        {
            if(lastGrown < lastWatered)// if it's been watered since it last grew
            {
                currentHealth -= ((float)(System.DateTime.Now - lastWatered).TotalSeconds - (pd.waterNeedHours * 3600 * .9f)) / (pd.deathTime * 1800);//kill by the amount of time since last below 90% thirst
            }
            else if((float)(lastGrown - lastWatered).TotalSeconds < pd.waterNeedHours * 3600 * .5f)//if last grown within half a water cycle of last watered
            {
                float start = (float)(lastGrown - lastWatered).TotalSeconds;
                currentHealth -= ((float)(System.DateTime.Now - lastGrown).TotalSeconds - ((pd.waterNeedHours * 3600 * .9f) - start)) / (pd.deathTime * 1800);//calc time since last at 90% thirst
            }
            else //watered way before last grown
            {
                currentHealth -= (float)(System.DateTime.Now - lastGrown).TotalSeconds / (pd.deathTime * 1800); //kill by the amount of time since last grown
            }
        }


        
        //sunlight
        float sunDiff = Mathf.Abs(currentSunlight - pd.sunNeed);//bigger means less ideal sunlight (0-100)
        if(sunDiff < 15)//healing boost
        {
            currentHealth += (float)(System.DateTime.Now - lastGrown).TotalSeconds / (pd.deathTime * 1800 * 3);
            //this isnt strong enough to keep a thirsty plant alive
        }
        else
        {
            currentHealth -= ((float)(System.DateTime.Now - lastGrown).TotalSeconds * ((sunDiff - 35) / 45)) / (pd.deathTime * 1800);//use magic numbers to kill based on how bad the sunlight is
            //this is strong enough to slowly kill a badly lit plant even if kept watered
        }

        if (currentHealth < -.99f)
        {
            hasDied = true;
        }
        else if(currentHealth > -.95f && hasDied)
        {
            hasDied = false;
            currentGrowth *= .7f;
        }

        currentHealth = Mathf.Clamp(currentHealth, -1f, 1f);
        if (debugMode)
            statusText.text = currentHealth.ToString("F2");
        else
            statusText.text = "";
    }
}
